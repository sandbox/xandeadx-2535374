<?php

class views_handler_sort_multicurrency_price extends views_handler_sort {
  function query() {
    $table_alias = $this->ensure_my_table();

    $conversion_settings = variable_get('commerce_multicurrency_conversion_settings', FALSE);
    $default_currency = commerce_default_currency();
    $currency_column_name = $this->definition['field_name'] . '_currency_code';

    $case_items = array();
    foreach ($conversion_settings[$default_currency]['rates'] as $currency_code => $rate_info) {
      $case_items[] = "WHEN '{$currency_code}' THEN {$rate_info['rate']}";
    }
    $case_items = implode("\n", $case_items);

    $sort = "{$table_alias}.{$this->real_field} / (CASE {$table_alias}.{$currency_column_name} {$case_items} ELSE 1 END)";

    $this->query->add_orderby(NULL, $sort, $this->options['order']);
  }
}
